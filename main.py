# SPDX-FileCopyrightText: 2020 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: MIT

import json
import click
import numpy

@click.group()
def cli():
    click.echo('My Custom Application')

@cli.command()
def count_datapoints():
    with click.open_file('data.json', 'r') as f:
        datapoints = json.load(f)
        click.echo(f'datapoint count = {len(datapoints)}')

@cli.command()
def analyse_data():
    with click.open_file('data.json', 'r') as f:
        datapoints = json.load(f)
        click.echo(f'height = {numpy.average([point["height"] for point in datapoints])}')
        click.echo(f'weight = {numpy.average([point["weight"] for point in datapoints])}')

if __name__ == '__main__':
    cli()
