# SPDX-FileCopyrightText: 2020 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: MIT

# (1)
FROM python:3.8.5

# (2)
WORKDIR /opt/my-project

# (3)
COPY . /opt/my-project

# (4)
RUN pip install -r requirements.txt

# (5)
ENTRYPOINT [ "python3", "main.py" ]
