<!--
SPDX-FileCopyrightText: 2020 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: MIT
-->

# A Sample Docker Project

This is a sample project used to demonstrate some ideas to do with Docker in a series of posts about using Docker for scientific applications.
